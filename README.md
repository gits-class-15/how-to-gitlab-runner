# Gitlab Runner Labs

This is step how to Gitlab Runner

## Before you Begin

1. **Deploy your Apps (Continuous Deployment)**
    > Includes Depedency to build Apps like : npm, composer , cli etc ..
	> This only application based on Continuous Deployment like : Web Application or API


3. **Create Step Pipeline**
	This means you create big step to release your apps, and then you can custom your flow pipeline to release your apps
	Example : 
```mermaid
graph LR
A(Build) --> C(Test)
C(Test) --> B(Deploy/Release)
```
4. **Create Step Procedure Build and Deployment or Etc based on Pipeline you build**
	This step is procedure of pipeline you create based on  ***how your app can run or released***
	Example Build VueJS :
	1. ```$ npm install```
	2. ```$ npm run build```
	3. ```$ cp ./dist/* /var/www/html``` (Using Apache Web Server)
	
5. **Create Repository and Push your code to repository or you already have one** 
	Push  your code to repository gitlab create project and push your code see 
	

## Step How to CI/CD at Gitlab Runner

1. Install Gitlab Runner for Agent : [Go to Section](##Install-Gitlab-Runner)
3. Registering your Gitlab Runner Runner on Project Repository : [Go to Section](##Registering-Runner-to-Repository)
5. Create gitlab-ci.yml to Repository and Scripting your Pipeline and Procedure Deploy : [Go to Section](##Example-.gitlab-ci.yml-Configuration-CI/CD-for-Vue-JS)
6. First CI/CD wi'll be running
7. Try to Change your Code !

## Install Gitlab Runner

> You can see detail here : [Gitlab Runner Installation](https://docs.gitlab.com/runner/install/)

### Linux Installation

1.	Add binaries to system 
	```copy
	sudo curl -L  --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
	```
2.	Give Permission
	```
	sudo chmod +x /usr/local/bin/gitlab-runner
	```
3. Create Gitlab User
	```
	sudo useradd --comment  'GitLab Runner'  --create-home gitlab-runner --shell /bin/bash
	```
4.	Install Gitlab Runner
	```
	sudo gitlab-runner install  --user=gitlab-runner --working-directory=/home/gitlab-runner
	```
5.	Run service gitlab-runner
	```
	sudo gitlab-runner start
	```
6. Set gitlab-runner User as Sudoers :
    ```
    sudo usermod -a -G sudo gitlab-runner
    ```
    > This is not recommended , i suggest to configure access on user gitlab-runner
7. Remove password Sudoers : 
   * Execute : ```sudo visudo```
   * Add the bottom of file : ```gitlab-runner ALL=(ALL) NOPASSWD: ALL```
   * Exit : `ctrl` + `x` -> `Enter`
   > This step is executed if step number six is executed

### Windows Installation

>You can see detail here : [Installation Gitlab Runner on Mac OS ](https://docs.gitlab.com/runner/install/osx.html)

1.	Create folder for gitlab-runner ex : `C:\Gitlab-Runner`

	![alt text](https://res.cloudinary.com/dodsvu9dx/image/upload/v1562291977/createfoldergitlabrunner.png)

2.	Download Runner for Windows
	[Windows 64-Bit](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe) [Windows 32-Bit](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-386.exe)
	
3. Copy paste program to the folder that has been created and rename the program to 'gitlab-runner'
![alt text](https://res.cloudinary.com/dodsvu9dx/image/upload/v1562291977/copypastegitlabrunnerandrename.png)

4.	Add program to system variable PATH (Windows 10)
You can see detail here : [How can I add a new folder to my system path? ](https://www.itprotoday.com/cloud-computing/how-can-i-add-new-folder-my-system-path)
Windows Start -> Search `environment variable` and Enter -> Click `Environment Variables ...` -> Search `Path` at System Variables and Double Click -> Click new and add folder path that you have been created Ex : `C:\Gitlab-Runner`-> Click `Ok` -> Click `Ok`
![ala text](https://res.cloudinary.com/dodsvu9dx/image/upload/v1562291977/EnvironmentVariableProgramGitlabRunner.png)

5. Open Terminal with Role **Administrator**

5. Test Gitlab Runner by Execute :
	`~ gitlab-runner`
6. Install Service Gitlab Runner by Execute :
    `~ gitlab-runner install --working-directory "<gitlab-runner folder>"`
    > `<gitlab-runner folder>` is the folder that you have created Ex : `C:\\Gitlab-Runner`
    
7. Start  Gitlab Runner Service by Execute : 
   `~ gitlab-runner start`

   
### Mac OS Installation 
You can see detail here : [Installation Gitlab Runner on Mac OS ](https://docs.gitlab.com/runner/install/osx.html)

## Registering Runner to Repository

1.	**See your Gitlab Runner Token**
    Go to your Repository -> Settings -> CI/CD -> Click `Expand` Runner Section
    ![alt text](https://res.cloudinary.com/dodsvu9dx/image/upload/v1562291977/runnertoken.png)

2. **Registering Runner**
    * Open your Terminal -> Execute : `~ gitlab-runner register`
    * Copy URL on `Specify the following URL during the Runner setup:` from Gitlab Runner Token  and Paste it -> Enter
    * Copy URL on `Use the following registration token during setup:` from Gitlab Runner Token and Paste it -> Enter
    * Type Laptop/PC or Server Description -> Enter
    Ex : `Laptop Farhan`
    * Type tag runner for identify Runner -> Enter
    Ex : `my-agent-laptop`
    * Type : `shell` -> Enter
    
    ![alt text](https://res.cloudinary.com/dodsvu9dx/image/upload/v1562291977/registeringrunner.png)

3. **Check your Runner** 
    Go to your Repository -> Settings -> CI/CD -> Click `Expand` Runner Section
    ![alt text](https://res.cloudinary.com/dodsvu9dx/image/upload/v1562291977/runnerhasbeenregistered.png)

> If runner activated and your runner Laptop/PC or Server is green, its mean runner is ready


## Example .gitlab-ci.yml Configuration CI/CD for Vue JS

###


1. **Go to to Your Repository**

2. **Click Web IDE**

3. **Create file and Commit** 

    Example VueJS :
    ```yaml
    stages :
      - test
      - develop
      - production

    Test:
      stage: test
      script:
        - npm install
        - npm run test:unit
      only:
        - master
      tags:
        - <your tag agent>

    Local:
      stage: develop
      script:
        - npm install
        - npm run build
        - cp -R .\dist\* C:\xampp\htdocs\
      only:
        - master
      tags:
        - gits-class-farhan

    Production:
      stage: production
      script:
        - npm install
        - npm run build
        - cp -R ./dist/* /var/www/html
      only:
        - master
      tags:
        - <your tag agent production>
      when: manual

    ```